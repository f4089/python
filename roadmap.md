
# Roadmap Python for Backend

## 5 lessons

1. Python Basics
    1.1. Data Structures
    1.2. Functions
        1.2.1. Arguments
        1.2.2. Return
        1.2.3. Scope
        1.2.4. Lambda
2. Classes
    2.2. Inheritance
    2.3. Polymorphism
    2.4. Abstraction
    2.5. Encapsulation
    2.6. Composition

***Nota: Git***
3. Decorators
    3.1. Decorators
    3.2. Decorator Arguments

------- 
## 3 lessons
4. Advanced Python
    4.1. Python Packages
    4.2. Python Modules
    4.3. Regular Expressions
    4.4. Gererator Expressions(yield)

----
## 2 lessons
6. Python Senior
    6.1. Unittest
    6.2. Pytest

----
## 6 lessons
### Introducción al Backend
7. Web Development
    7.1. Flask -> APIs, Webhooks, API REST. 
    7.2. Django --> APIs, Webhooks, API REST.
    7.3. Bottle --> APIs, Webhooks, API REST.
----
## 6 lessons
8. Webscraping
    8.1. Requests
    8.2. BeautifulSoup
    8.3. Scrapy --> Framework
    8.4. Selenium -> Navegadores Sin Cabeza (Headless)