"""
Lista

Es la estructura de datos más simple Python. Se utiliza para almacenar varios tipos
de datos, y se crean con corchetes. Un tipo de dato mutable. 

"""
#          0  1  2  3  4
lista_1 = [1, 2, 3, 4, 5]

# Referenciar listas por posición
print(lista_1[0])

# Existencia de un elemento en la lista
nombres = ["Franzua", "Juancito", "Lesli", "Tobi", "Mozilla"]
nombre_buscar = "Mozilla"

print(nombre_buscar in nombres)

# No existencia de un elemento en la lista
nombre_buscar = "Franzua"
print(nombre_buscar not in nombres)

# Longitud de una lista
print(len(nombres))

# Añadir elementos al final de la lista
nueva_nombre = "Miriam"
nombres.append(nueva_nombre)
print(nombres)

# Añadir elementos a la lista
nuevo_nombre = "Juan"
 #     .insert(posicion, elemento)
nombres.insert(0, nuevo_nombre)
print(nombres)

# Modificar valor de un elemento de la lista

# <list>[index] = new_value
nombres[0] = "Kenny"
print(nombres)

# Combinar listas

vocales_1 = ["a", "e", "i"]
vocales_2 = ["o", "u"]

print("".center(100, "-"))
vocales_1.extend(vocales_2)
print(vocales_1)

#  Eliminar elementos de la lista por contenido
vocales_1.remove("o")


# Eliminar elementos de la lista por posición
# El método pop, aparte de elminar el elemento de la lista, devuelve el elemento eliminado.
valor_eliminado = vocales_1.pop(0)
print(f"Valor eliminado: {valor_eliminado}\nLista Fin: {vocales_1}")

# Eliminar todos los elementos de una lista
letras_abecedario = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]

letras_abecedario.clear()
print(letras_abecedario)

# Contar la cantida de veces que aparece un elemento en la lista

nombre_eli = "Eliana Miriam"

letra_buscar = "a"
numero_veces = nombre_eli.count(letra_buscar)

print(f"La letra \"{letra_buscar}\" aparece {numero_veces} veces en {nombre_eli}")

# Saber el indice de un elemento en la lista
letras = ["A", "B", "C"]
indice = letras.index("B")
print(f"El indice de 'B' es {indice}")

