"""
Formattings Strings

- Normal Strings
- Format Strings
- f strings -> Recomendación 

"""


# Normal String - No recomiendo

nombre: str = "Kenny"
edad: int = 27
profesion: str = "industrial"

cadena = "Hola, soy " + nombre + ". Tengo " + str(edad) + " años. " + "Estudio ingeniería " + profesion

# print(cadena)

# Format String

#* Usando variables
nombre: str = "Kenny"
edad: int = 27
profesion: str = "ingeniería industrial"


cadena = "Hola!!! Soy {}. Tengo {} años. Estudio {}".format(nombre, edad, profesion) # Alt + Shift + tecla abajo
cadena_1 = "Hola!!! Soy {2}. Tengo {1} años. Estudio {0}".format(nombre, edad, profesion)

# print(cadena)
# print(cadena_1)

#* Usando listas

lista = ["Kenny", 27, "ingeniería industrial"]

#! No es un buena práctica
# cadena_lista = "Soy {}. Tengo {} años. Estudio {}".format(lista[0], lista[1], lista[2])
#? Mejor práctica
cadena_lista = "Soy {0}. Tengo {1} años. Estudio {2}".format(*lista)
# print(cadena_lista)

#* Usando Diccionarios

persona = {
    "name": "Kenny",
    "last_name" : "Odar",
    "age" : 20,
    "degree" : "Ingeniería de Sistemas" 
}

dicc_cadena = "Hola!!! Soy {name} {last_name}. Tengo {age} años. Estudio {degree}".format(**persona)
# print(dicc_cadena)


# f-strings -> Mejor opción - Apareció en la versión 3.6 de Python.

# Con variables
nombre: str = "Kenny"
edad: int = 27
profesion: str = "ingeniería industrial"

cadena_f = f"Mi nombre es {nombre}. Tengo {edad} años. Estudio {profesion}. La suma de 4 y 5 es: {4+5}"
# print(cadena_f)

# Con listas

persona_lista = ["Kenny", 27, "ingeniería industrial"]
cadena_lista_f = f"Mi nombre es {persona_lista[0]}. Tengo {persona_lista[1]} años. Estudio {persona_lista[2]}"


# Con diccionarios

persona_dicc_f = {
    "name": "Kenny",
    "last_name" : "Odar",
    "age" : 20,
    "degree" : "Ingeniería de Sistemas" 
}

# f-docstrings
cadena_dicc_f = f"""
    Mi nombre es {persona_dicc_f.get("name")} {persona_dicc_f.get("last_name")}.
    Tengo {persona_dicc_f.get("age")} años.
    Estudio {persona_dicc_f.get("degree")}.
"""

# print(cadena_dicc_f)


#? Manipulando Strings

#! Longitud de un string
ejemplo_1 = ""
ejemplo_2 = " "
ejemplo_3 = "Hola Mundo"

# print(f"La longitud de ejemplo_1 es: {len(ejemplo_1)}") # Klaus: 1 | Juan: 0
# print(f"La longitud de ejemplo_2 es: {len(ejemplo_2)}") # Klaus: 1 | Juan: 1
# print(f"La longitud de ejemplo_3 es: {len(ejemplo_3)}") # Klaus: 10 | Juan: 10

#! Saber si un string está dentro de otro -> bool  

string_1 = "China encuentra evidencia de una civilización extraterrestre"
string_2 = "China"

# print(string_2 in string_1)

#! Saber si un string no está dentro de otro -> bool

string_3 = "Alianza Campeón"
string_4 = "Melgar"

# print(string_4 not in string_3)

#! Repetir un string una cantidad n de veces

n = 10
string_5 = "Python"

# print(n*string_5)

#! Obtener un caracter de un string
#    0123456789ABC
#                -1
x = "Soy un string"
# print(x[0]) # Klaus: S | Juan: Soy | Eli: S
# print(x[1]) # Klaus: o | Juan: un | Eli: o
# print(x[-1]) # Klaus: g | Juan: None | Eli: g

#! Obtener un grupo de caracteres
c = "Soy un-string"
# Desde la posición 3 hasta una posición antes que el 10
# print(c[3:10])
# print(len(c[3:10]))

#! Obtener un grupo de caracteres con un intervalo
# <string>[a:b:k]
#    0123456789ABC
d = "Soy-un-string"
# print(d[0:10:2])
# print(len(d[0:10:2]))
    #? Invertir un string
# print(d[::-1])

#! Obtener el caracter menor de un string
a = "Soy un string"
print(f"El menor es: '{min(a)}' ")
# print(f'El menor es: "{min(a)}" ')

#! Obtener el caracter mayor de un string
b = "Soy un string"
print(f"El menor es: '{max(b)}' ")