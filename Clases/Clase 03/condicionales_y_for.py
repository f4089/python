# Programación Sincrona -> Python

# Condicionales
# = : Asignación
# == : Comparación (Igual que)

numero: int = 6
print(numero == 3)

if numero == 3:
    print("El número es 3")
elif numero == 4:
    print("El número es 4")
elif numero == 5:
    print("El número es 5")
else:
    print("No")


# # Otra manera
numero: int = 3

# Compressed If
# if_action if value else else_action
print("Número 3") if numero == 3 else print("No es 3")


# Iteradores

# for

lista = [1, 2, 3, 4, 5]

"""

range(first_number: Optional, last_number: Obligatory, Step: Optional)

"""


for i in range(3, 20, 4):
    #3, 7, 11, 15, 19
    print(i)

print("Todo Listo")

for i in "editor":
    print(i)

for i in "editor":
    # print("Esto si imprime")
    if i == "e":
        continue
    elif i == "d":
        print("Esto es un D en elif 1")
    print(i)

notas = ["AD", "A", "", "C", "F" ]

for nota in notas:
    if not(nota):
        print("Nota Incompleta")
        continue
    print(nota)

for i in range(10):
    for j in range(10,20):
        if j % 3 == 0:
            continue
        print(j)
    print(i, end=" ")

print(bool(notas[0])) # -> True
print(bool(notas[1])) # -> True
print(bool(notas[2])) # -> False
print(bool(notas[3])) # -> True
print(bool(notas[4])) # -> True

"""
? ¿Cuando Python toma a un valor como booleano?

Python toma siempre a los valores como booleanos. Siempre y cuando sean:
- str -> Es True cuando el str está con caracteres. Es False cuando el string no tiene caracteres
- bool 
- data structure: -> Es True cuando la estructura tiene al menos un elemento. Es Falso cuando está vacio.
    - lista
    - tupla
    - sets
    - diccionarios
"""







