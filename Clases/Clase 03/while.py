# While

# letra = 65

# while letra < 91:
#     print(f"{chr(letra)}: {letra}")
#     # letra = letra + 1
#     letra += 1

# print("Todo Terminado")


# numero = 0
# while True:
#     if numero < 100:
#         if numero % 5 == 0:
#             print(numero)
#             numero += 1
#         else:
#             numero += 1
#             continue
#     else:
#         break

import random

while True:
    numero = random.randint(0, 100)
    if numero % 5 == 0:
        print(numero)
        break
    print(numero)



