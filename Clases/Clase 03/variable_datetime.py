import datetime

# datetime.date -> Usar variables para obtener las fechas
# datetime.time -> Usar variables para obtener las horas
# datetime.datetime -> Usar variables para obtener las fechas y horas

# Trabajando con Fechas

import datetime as dt

hoy = dt.date.today()

# print(f"""El día es: {hoy.day}\nEl mes es: {hoy.month}\nEl año es: {hoy.year}""")

eli_nacimiento = dt.date(2001, 5, 26)
# print(eli_nacimiento, f"La clase es: {type(eli_nacimiento)}")

# Trabajando con horas

import datetime as dt

# dt.time(hour, minute, second, microsecond)
hora = dt.time()
# print(f"La hora es: {hora}")

hora_1 = dt.time(21, 44, 35, 1)
# print(f"La hora es: {hora_1}", f"La clase es: {type(hora_1)}")

# Trabajando con Fechas y horas

import datetime as dt

hoy = dt.datetime.today()
print(f"La fecha y hora es: {hoy:%A-%B-%Y}", f"La clase es: {type(hoy)}")