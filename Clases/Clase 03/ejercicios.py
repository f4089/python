# Ejercicio 1

# monto: float = float(input("Ingrese el monto: "))
# tasa: float = float(input("Ingrese la tasa: "))
# tiempo: int = int(input("Ingrese el tiempo: "))

# prestamo = monto * (pow(1+tasa/100, tiempo))

# print(f"Con un monto de {monto} soles a una tasa anul de {tasa}% durante {tiempo}, se tendra {prestamo:.2f} soles")

# Ejercicio 2

# primer_bit = input("Ingrese el bit b0: ")
# segundo_bit = input("Ingrese el bit b1: ")
# tercer_bit = input("Ingrese el bit b2: ")
# cuarto_bit = input("Ingrese el bit b3: ")

# bit = primer_bit + segundo_bit + tercer_bit + cuarto_bit
# # "1001"

# # Binario a Decimal
# bit = int(bit, base=2)
# # int("1001") -> 1001}

# # Decimal a Binario
# bin(123)


# print(f"El valor decimal es {bit}")

# Ejercicio 3
velocidad: float = float(input("Introduza la velocidad de internet: "))

velocidad_bytes = (velocidad*pow(10,6))/8
tiempo_descarga = (80e+15)/velocidad_bytes
tiempo_descarga_dias = (6.4e+10)/86400
print(f"Se demorará {tiempo_descarga_dias:.2f} días")