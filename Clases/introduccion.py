"""
Python
----
Lenguaje de programación de alto nivel el cual usa un interpretador como un intermediario 
entre el usuario y el sistema operativo. Es fuertemente tipado.

"""

# Variables
"""
Python tiene las declaraciones de variables llamadas Literales. Existen 4 tipos:
- Literal Number
- Literal String
- Literal Bytes
- Literal Flotante
"""

# Enteros

a = 1 # Entero
b = 1.1 # Flotante
c = .99 # Flotante
d = -1 # Negativo
e = 12 + 2j # Complejo

# String 
word = "Hola mundo"
word_2 = "Hola, estás en Python"
word_3 = "127.0.0.1"
word_3 = "" # String Vacio -> False
word_4 = "Hola\nMundo" # Salto de linea
print(word_4)

# Booleanos
x = True
y = False | True

## OPERACIONES

# Suma
suma = 1+1
# Resta
resta = 1-1
# Mutiplicación
mult = 1*2
# División
# div = 1/0
# print(div)

# Infito
## Método 1
infinito_1 = float("inf")
print(infinito_1)

## Método 2
import math
positive_infinity = math.inf
negative_infinity = -math.inf
print(f"Infinito Positivo: {positive_infinity}") # Alt + Shift + Tecla Abajo
print(f"Infinito Negativo: {negative_infinity}")

## Método 3
from decimal import Decimal

positive_infinity = Decimal("Infinity")
negative_infinity = Decimal("-Infinity")
print(f"Infinito Positivo: {positive_infinity}")
print(f"Infinito Negativo: {negative_infinity}")

# Módulo(%)

residuo = 11 % 3
print(residuo)

# Exponente(**)

potencia = 3**2
indice = 3
potencia = pow(3, indice)
print(potencia)

# Floor division(//)
# Redondea hacia abajo
a = 11/3
print(a)
number = 11//3
print(number)