# Built-in Functions

# Valor Absoluto
print(abs(-5))
print(abs(5))

# bin()
print(bin(5))
print(bin(20))

# float()

# hex()
print(hex(5))
print(hex(20))

# int()

# max()
print(max(5, 10, 15, 1, 100))

# min()
print(min(5, 10, 15, 1, 100))

# oct()
print(oct(5))
print(oct(20))

import math

# math.acos(x)
print(math.acos(-1))
print(f"math.pi: {math.pi:.100f}")

#math.atan() 



