import random

def adivinador(numero:int) -> bool:
    if 1<=numero<=50:
        solution = random.randint(1,50)
        if solution == numero:
            print(f"Tu número es {solution}")
            return True
        else:
            return False
    else:
        print("El numero debe estar entre 1 y 50")
        return False



if __name__ == '__main__':
    flag = 1
    while True:
        numero = int(input('Ingrese un numero: '))
        a = adivinador(numero)
        if a:
            print(f"Has jugado {flag} veces")
            break
        if not(a):
            confirmation = input(f"""No le atiné. 
                                 ¿Deseas jugar de nuevo? (Y/N): """)
            if (confirmation == "Y" or confirmation == "y"):
                flag+=1
            if (confirmation == "N" or confirmation == "n"): 
                print(f"Has jugado {flag} veces")
                break


